/*
 
   Find minimum Equilibrium index

   */

import java.io.*;

class Equilibrium{

	int equIndex(int[] arr){
	
		for(int i = 1 ; i < arr.length ; i++){
		
			arr[i] = arr[i - 1] + arr[i]; 
		}

		int lsum = 0;
                int rsum = 0;
		int n = arr.length;

		for(int i = 0; i < arr.length ;i++){
		
			if(i == 0){
				lsum = 0;
			}else{
				lsum = arr[i - 1];
			}

			if(i == n-1){
				rsum = 0;
			}else{
				rsum = arr[n-1] - arr[i+1] + 2;
			}

			if(lsum == rsum){
				return i;
			}

		}
		return -1;
	}

	public static void main(String[] args)throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Array Size");
		int size = Integer.parseInt(obj.readLine());

		System.out.println("Enter the Array Elements");
		int arr[] = new int[size];

		for(int i = 0;i<arr.length;i++){

			arr[i]=Integer.parseInt(obj.readLine());

		}

		Equilibrium eq = new Equilibrium();
		int eqindex = eq.equIndex(arr);
		System.out.println(eqindex);
	}
}
