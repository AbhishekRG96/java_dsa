






import java.io.*;

class ShortSubArray{

	int minLength(int[] arr){
		
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;

		for(int i = 0; i< arr.length ;i++){
		
			if(arr[i] >= max){
				max = arr[i];
			}

			if(arr[i] <= min){
				min = arr[i];
			}
		}	

		int minindex = 0;
		int maxindex = 0;
		int length = Integer.MAX_VALUE;
		int minlength = Integer.MAX_VALUE;
		int flag1 = 0;
		int flag2 = 0;
			
		for(int i = 0; i < arr.length ; i++){
		
			if(arr[i] == min){
				minindex = i;
				flag1 = 1;
			
			}else if(arr[i] == max){
                                minindex = i;
				flag2 = 1;
                        }

			if(flag1 == 1 && flag2 == 1){
			
				if(minindex > maxindex){
					length = minindex - maxindex + 1;
				}else{
					length = minindex - maxindex + 1;
				}
				
				if(length < minlength){
					minlength = length;
				}
			}
		}
		if(minlength >= 0){
			return minlength;
		}
		return -1;

	}
	

	public static void main(String args[])throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Array Size");
		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter the Array Elements");
		int arr[] = new int[size];

		for(int i = 0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());

		}

		ShortSubArray obj = new ShortSubArray();
		int minlength = obj.minLength(arr);

		System.out.println("Min length is : " + minlength);
	}
}
