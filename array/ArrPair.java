



import java.io.*;

class ArrPair{

	static int pairCount(char[] arr){
	
		int countofa = 0;
		int countofb = 0;
		int count = 0;

		for(int i = 0 ; i < arr.length ; i++){

				if(arr[i] == 'a'){
					countofa++;
				}
				if(arr[i] == 'b'){
					countofb = countofb + countofa;
				}
				if(arr[i] == 'c'){
                                        count = count + countofb;
                                }

			}
		return count;
	}

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Array Size");
		int size = Integer.parseInt(obj.readLine());

		System.out.println("Enter the Array Elements");
		char arr[] = new char[size];

		for(int i = 0;i<arr.length;i++){

			arr[i] = (char)obj.read();
			obj.skip(1);

		}

		int paircount = pairCount(arr);

		System.out.println("Pair count is : " + paircount);
	}
}
