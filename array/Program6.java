/*
 
6.
sum of pair
i/p - 1
[8,4,1,3,9,2,6,7]
8 + 7 = 15 ;
4 + 6 = 10 ;
1 + 2 = 3 ;
3 + 9 = 12 ;
sum = 40;
i/p -2
[1,2,3,4,5]
1 + 5 = 6
2 + 4 = 6
3 = 3
sum = 15
T.C = o(N)
 
 */



import java.io.*;

class Program6{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter Size of Array : ");
                int size = Integer.parseInt(br.readLine());

                int[] arr = new int[size];

                System.out.println("Enter " + size +" Array Elements : ");

                for(int i =0;i<arr.length ; i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }

               

                for(int i = 0; i < (arr.length+1)/2 ;i++){
                        
			int j =arr.length-i-1;
			if(i == arr.length/2){
                                j++;
				arr[j] = 0;
				//System.out.println(arr[i]);
				//break;
                        }
			System.out.println(arr[i] + arr[j]);
		}
	}
}

