/*
 
  WAP to print prime number in a range. 
 */
import java.util.*;

class Program5{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter range ");
		int start = sc.nextInt();
		int end = sc.nextInt();
		
		for(int i = start ; i <= end; i++){
		
			if(i < 2){
				i = 2;
			}	

			int count = 0;
			for(int j = 2 ; j <= i/2 ; j++){ 
			
				if(i % j == 0){
					count++;
					break;
				}
			}
			if(count == 0){
				System.out.print(i + " ");
			}
		}
	}
}
