/*
 
   0
   3 8
   15 24 35
   48 63 80 99

 */
import java.util.*;

class Program2{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter rows ");
		int rows = sc.nextInt();

		int num = 0;
		int x = 3;

		for(int i = 1 ; i <= rows; i++){
			
			for(int j = 1 ; j <= i ; j++){ 
			
				System.out.print(num + " ");
				num = num + x;
				x = x + 2;
			}
			System.out.println("");
			
		}
	}
}
