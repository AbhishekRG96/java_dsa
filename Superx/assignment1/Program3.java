/*
 
   5
   6 8
   7 10 13 
   8 12 16 20
   9 14 19 24 29

 */
import java.util.*;

class Program3{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter rows ");
		int rows = sc.nextInt();

		int num = 0;
		int x = 0;

		for(int i = 1 ; i <= rows; i++){
			
			num = rows + x;
			for(int j = 1 ; j <= i ; j++){ 
			
				System.out.print(num + " ");
				num = num + i;
				
			}
			System.out.println("");
			x++;
		}
	}
}
