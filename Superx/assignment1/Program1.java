/*
 
 1 2 3 4 
 4 5 6 7 
 6 7 8 9 
 7 8 9 10

 */
import java.util.*;

class Program1{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter rows ");
		int rows = sc.nextInt();

		int num = 1;
		int x = 0;

		for(int i = 1 ; i <= rows*rows; i++){
			
			if( i % rows == 0){
				
				System.out.println(num + " ");
				num = num - x;
				x++;
			}else{
				System.out.print(num + " ");
				num++;
			}
		}
	}
}
