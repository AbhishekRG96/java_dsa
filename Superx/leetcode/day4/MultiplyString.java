/*
 Day-4
Multiply Strings (LeetCode - 43)
Given two non-negative integers num1 and num2 represented as strings,
return the product of num1 and num2, also represented as a string.
Note: You must not use any built-in BigInteger library or convert the inputs
to integers directly.
Example 1:
Input: num1 = "2", num2 = "3"
Output: "6"
Example 2:
Input: num1 = "123", num2 = "456"
Output: "56088"
Constraints:
1 <= num1.length, num2.length <= 200
num1 and num2 consist of digits only.
Both num1 and num2 do not contain any leading zero, except the number 0
itself.
 */

import java.util.*;

class MultiplyString{

	static String product(char[] str1 , char[] str2){
	
		int[] arr1 = new int[str1.length];
	       	int[] arr2 = new int[str2.length];	
		
		int num1 = 0;
		int num2 = 0;

		for(int i = 0; i < str1.length ; i++){

			arr1[i] = str1[i] - 48;
			num1 = num1 *10 + arr1[i];
		}

		for(int i = 0; i < str1.length ; i++){

                        arr2[i] = str2[i] - 48;
			 num2 = num2 *10 + arr2[i];
                }

		int mult = num1 * num2;
		String multstr = Integer.toString(mult);
		
		return multstr;
	}


	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter first string : ");
		String string1 = sc.next();

		System.out.println("Enter second string : ");
                String string2 = sc.next();

		char[] str1 = string1.toCharArray();
		char[] str2 = string2.toCharArray();


		String mult = product(str1 , str2);
		System.out.println(mult);
	}
}

