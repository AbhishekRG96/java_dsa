/*
 Reverse String (LeetCode - 344)
Write a function that reverses a string. The input string is given as an array
of characters s.
You must do this by modifying the input array in-place with O(1) extra
memory.

Example 1:
Input: s = ["h","e","l","l","o"]
Output: ["o","l","l","e","h"]
Example 2:
Input: s = ["H","a","n","n","a","h"]
Output: ["h","a","n","n","a","H"]

Constraints:
1 <= s.length <= 10
5

s[i] is a printable ascii character.

*/

import java.util.*;

class ReverseStringArray{

	static String[] revStrArr(String[] strarr){
	
		int start = 0;
		int end = strarr.length -1;

		for(int i = 0 ; start < end ; i++){
		
			String st = strarr[start];
			strarr[start] = strarr[end];
			strarr[end] = st;

			start++;
			end--;

		}
		return strarr;
	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter String size : ");
		int size = sc.nextInt();

		String[] strarr = new String[size];

		System.out.println("Enter string : ");

		for(int i = 0 ; i < size ; i++){
		
			strarr[i] = sc.next();
		}

		String[] strarr2 = revStrArr(strarr);

		for(String s : strarr2){
			System.out.print(s + " ");
		}
		System.out.println();
	}
}
