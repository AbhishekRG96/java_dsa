/*
 Two Sum (LeetCode 1)
Given an array of integers nums and an integer target, return indices of the two
numbers such that they add up to target.
You may assume that each input would have exactly one solution, and you may not use
the same element twice.
You can return the answer in any order.
Example 1:
Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
Example 2:
Input: nums = [3,2,4], target = 6
Output: [1,2]
Example 3:
Input: nums = [3,3], target = 6
Output: [0,1]
Constraints:
2 <= nums.length <= 104
-109 <= nums[i] <= 109
-109 <= target <= 109
Only one valid answer exists.

*/

import java.io.*;

class TwoSum{

	int[] twoSum(int[] arr , int target){
	
		int[] twoarr = new int[2];
		
		for(int i = 0; i < arr.length ;i++){

                        for(int j = i + 1 ; j < arr.length ; j++){

                                if(arr[i] + arr[j] == target){
                                        
					twoarr[0] = arr[i];
					twoarr[1] = arr[j];

					return twoarr;
                                }
				
                        }
                }
		return twoarr;
		
	}

	public static void main(String[] args)throws IOException{

        	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        	System.out.println("Enter Size of Array : ");
        	int size = Integer.parseInt(br.readLine());
        
        	int[] arr = new int[size];

        	System.out.println("Enter " + size +" Array Elements : ");
        	
		for(int i =0;i<arr.length ; i++){
            		arr[i] = Integer.parseInt(br.readLine());
        	}

		System.out.println("Enter target number : ");
                int target = Integer.parseInt(br.readLine());	

		TwoSum obj = new TwoSum();
		int[] twoarr = obj.twoSum(arr,target);

		for(int x : twoarr){
			System.out.print(x + " ");
		}
		System.out.println();
	}
}
