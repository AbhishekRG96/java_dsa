/*
 1. To Lower Case (Leetcode-709)
Given a string s, return the string after replacing every uppercase
letter with the same lowercase letter.
Example 1:
Input: s = "Hello"
Output: "hello"
Example 2:
Input: s = "here"
Output: "here"
Example 3:
Input: s = "LOVELY"
Output: "lovely"
Constraints:
1 <= s.length <= 100
s consists of printable ASCII characters
 */
 import java.util.*;

class LowerCase{

	String toLowerCase(char[] str1){
	
		for(int i = 0; i < str1.length ;i++){
		
			if((str1[i] >= 'A' && str1[i] <= 'Z')){
				
				str1[i] = (char)(str1[i] + 32);
			}

		}
		String str = new String(str1);
		return str;
	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter string : ");
		String string1 = sc.next();

		char[] str1 = string1.toCharArray();

		LowerCase obj = new LowerCase();

		String str = obj.toLowerCase(str1);
		System.out.println(str);
		
		
	}
}

 
