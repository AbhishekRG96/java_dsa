/*
 2. Find the Index of the First Occurrence in a String(LeetCode 28)

Given two strings needle and haystack, return the index of the first occurrence of needle
in haystack, or -1 if needle is not part of haystack.
Example 1:
Input: haystack = "sadbutsad", needle = "sad"
Output: 0
Explanation: "sad" occurs at index 0 and 6.
The first occurrence is at index 0, so we return 0.
Example 2:
Input: haystack = "leetcode", needle = "leeto"
Output: -1
Explanation: "leeto" did not occur in "leetcode", so we return -1.

Constraints:
1 <= haystack.length, needle.length <= 104
haystack and needle consist of only lowercase English characters.
 */

import java.util.*;

class FirstOccur{

	int firstOcc(char[] str1 , char[] str2){
	
		for(int i = 0; i < str1.length ; i++){

                        int count = 0;

                        int flag = 0;

                        for(int j = 0 ;j < str2.length ; j++){

                                if(str1[i] == str2[j]){

                                        flag = 1;
                                }

                                if(flag == 1){

					if(i+j > str1.length){
						if(str1[i+j-1] == str2[j]){
                                                	count++;
                                      	 	}else{
                                                	flag = 0;
                                        	}
					}else{
                                        	if(str1[i+j] == str2[j]){
                                        	        count++;
                                        	}else{
                                               	flag = 0;
						}
					}
                                }else{
					break;
				}

                        }
                        if(count == str2.length){
                               	return i;
                        }
		}
		return -1;
	}

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter string : ");
		String string1 = sc.next();

		System.out.println("Enter string to find : ");
                String string2 = sc.next();

		char[] str1 = string1.toCharArray();
		char[] str2 = string2.toCharArray();
		
		FirstOccur obj = new FirstOccur();

		int index = obj.firstOcc(str1 , str2);
		System.out.println(index);
	}
}


