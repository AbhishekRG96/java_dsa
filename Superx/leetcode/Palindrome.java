/*
 1. Palindrome Number(LeetCode 9)
Given an integer x, return true if x is a palindrome, and false otherwise.
Example 1:
Input: x = 121
Output: true
Explanation: 121 reads as 121 from left to right and from right to left.
Example 2:
Input: x = -121
Output: false
Explanation: From left to right, it reads -121. From right to left, it becomes 121-.
Therefore it is not a palindrome.
Example 3:
Input: x = 10
Output: false
Explanation: Reads 01 from right to left. Therefore it is not a palindrome.

Constraints:
-231 <= x <= 231 - 1
 */

import java.util.*;

class Palindrome{

	boolean isPal(int num){
	
		int num2 = num;

		int rev = 0;
		while(num2 != 0){
		
			int rem = num2 % 10;
			num2 = num2 / 10;
			rev = rev * 10 + rem;
		}

		if(rev == num){
			return true;
		}
		return false;
	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number : ");
		int num = sc.nextInt();
		
		Palindrome obj = new Palindrome();
		boolean ispal = obj.isPal(num);

		System.out.println(ispal);

	}
}
