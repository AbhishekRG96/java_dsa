/*
 Que 5 : WAP to check whether the string contains characters other than
letters.
*/
import java.util.*;

class Program5{

	boolean isOtherChar(char[] str1){
	
		for(int i = 0; i < str1.length ;i++){
		
			int flag = 0;

			if((str1[i] >= 'a' && str1[i] <= 'z') || (str1[i] >= 'A' && str1[i] <= 'Z')){
				
				flag = 1;
			}

			if(flag == 0){
				return true;
			}
		}
		return false;
	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter string : ");
		String string1 = sc.next();

		char[] str1 = string1.toCharArray();

		Program5 obj = new Program5();

		boolean isotherchar = obj.isOtherChar(str1);
		
		if(isotherchar == true){
			System.out.println("Given string contains other character than letters");
		}else{
			System.out.println("Given string does not contains other character than letters");
		}
	}
}
