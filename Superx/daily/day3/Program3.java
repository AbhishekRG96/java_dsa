/*
 Que 3 : WAP to check whether the given no is a palindrome number or not.
 */

import java.util.*;

class Program3{

	boolean isPal(int num){
	
		int num2 = num;

		int rev = 0;
		while(num2 != 0){
		
			int rem = num2 % 10;
			num2 = num2 / 10;
			rev = rev * 10 + rem;
		}

		if(rev == num){
			return true;
		}
		return false;
	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number : ");
		int num = sc.nextInt();
		
		Program3 obj = new Program3();
		boolean ispal = obj.isPal(num);

		if(ispal == true){
			System.out.println("Palindrome");
		}else{
			System.out.println("Not Palindrome");
		}

	}
}


