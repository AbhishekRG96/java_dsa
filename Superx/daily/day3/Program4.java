/*
 Que 4 : WAP to print each reverse numbers in the given range
Input: start:25435
end: 25449
*/

import java.util.*;

class Program4{

	void reverse(int start , int end){

		for(int i = start ; i <= end ; i++){

			int num = i;
			int rev = 0;

			while(num != 0){

				int rem = num % 10;
				num = num / 10;
				rev = rev * 10 + rem;
			}

			System.out.println(rev);
		}
	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number range : ");
		int start = sc.nextInt();
		int end = sc.nextInt();

		Program4 obj = new Program4();
		obj.reverse(start,end);
	}
}
