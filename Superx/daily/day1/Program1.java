/*
Que 1 : WAP to print the following pattern
Take input from user
1 2 3 4
2 3 4 5
3 4 5 6
4 5 6 7 
 */

import java.util.*;

class Program1{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number of rows : ");
		int row = sc.nextInt();

		for(int i = 1 ; i <= row ; i++){
		
			for(int j = i ; j < i + row ; j++){
			
				System.out.print( j + " ");
			}
			System.out.println();
		}
	}
}
