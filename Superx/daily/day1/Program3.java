/*
 Que 3 : WAP to check whether the given no is odd or even
 */

import java.util.*;

class Program3{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter number of rows : ");
                int num = sc.nextInt();

		if(num % 2 == 0){
			System.out.println("Given number is even");
		}else{
			System.out.println("Given number is odd");
		}
	}
}


