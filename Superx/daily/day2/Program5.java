/*
 Que 5 : WAP to check whether the string contains vowels and return
the count of vowels.
 */

import java.util.*;

class Program5{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter string : ");
		String strin = sc.next();

		char[] str = strin.toCharArray();
		int count = 0;
		for(int i = 0; i < str.length ; i++){
		
			if(str[i] == 'a' || str[i] == 'e' || str[i] == 'i' || str[i] == 'o' || str[i] == 'u'){
				count++;
			}

			if(str[i] == 'A' || str[i] == 'E' || str[i] == 'I' || str[i] == 'O' || str[i] == 'U'){
                                count++;
                        }
		}
		if(count >0){
			System.out.println("Given String contains " + count + " vowels ");
		}else{
			System.out.println("Given string does not contains vowels ");
		}
	}
}
