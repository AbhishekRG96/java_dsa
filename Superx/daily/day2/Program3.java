/*
 Que 3 : WAP to check whether the given no is prime or composite
 */

import java.util.*;

class Program3{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number : ");
		int num = sc.nextInt();

		if(num <=1){
	  		System.out.println(num + " is not prime nor composite");
		}else{
			int count = 0;

			for(int i = 2; i <= num/2 ;i++){
	
				if(num % i == 0){
					count++;
				}
			}

			if(count == 0){
       	                	System.out.println(num + " is prime");
                	}else{
				System.out.println(num + " is composite");
			}
		}
	}
}
