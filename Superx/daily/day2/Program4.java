/*
Que 4 : WAP to print the composite numbers in the given range
Input: start:1
end:100
 */

import java.util.*;

class Program4{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter range : ");
		int start = sc.nextInt();
		int end = sc.nextInt();

		for(int j = start ; j <= end ; j++){
			 
			if(j ==1 ){
				j++;
			}
			int count = 0;
			
			for(int i = 2; i <= 10 ;i++){

				if(j != i && j % i == 0){
					count++;
				}	
			}

			if(count == 0){
				System.out.print(j + " ");
			}
		}
	}
}
