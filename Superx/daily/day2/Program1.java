/*
 Que 1 : WAP to print the following pattern
Take input from user
A B C D
B C D E
C D E F
D E F G
 */

import java.util.*;

class Program1{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter rows : ");
		int row = sc.nextInt();

		for(int i = 0; i < row ;i++){
		
			int x = 'A' + i;
			char ch = (char)x;

			for(int j = 1 ; j <= row ; j++){
			
				System.out.print(ch + " ");
				ch++;
			}
			System.out.println();
		}
	}
}
