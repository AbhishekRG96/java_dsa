/*
Que 4: WAP to print the sum of digits in a given range.
Input: 1 to 10
Input: 21 to 30 
*/

import java.util.*;

class Program4{

	static int sumOfRange(int start , int end){
	
		int sumofstart = start * (start + 1 )/2;
		int sumofend = end * (end + 1)/2;

		int sumofrange = sumofend - sumofstart + 1;

		return sumofrange;
	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter range : ");
		int start = sc.nextInt();
		int end = sc.nextInt();

		System.out.println(sumOfRange(start, end));
	}
}
