/*
 Que 1: WAP to print the following pattern
Take input from the user
A B C D
# # # #
A B C D
# # # #
A B C D

*/

import java.util.*;

class Program1{

	void printPattern(int row){

		for(int i = 1; i <= row ; i++){

			char ch = 'A';
			for(int j = 1; j <= row - 1 ;j++){

				if(i % 2 == 1){
					System.out.print(ch + " ");

					if(j != row){
						ch++;
					}

				}else{
					System.out.print("# ");
				}
			}
			System.out.println();
		}
	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter row :");
		int row = sc.nextInt();

		Program1 obj = new Program1();
		obj.printPattern(row);

	}
}
