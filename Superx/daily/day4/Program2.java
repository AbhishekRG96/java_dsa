/*
Que 2: WAP to print the following pattern
Take row input from the user
A
B A
C B A
D C B A
*/   
import java.util.*;

class Program2{

	void printPattern(int row){
	
		for(int i = 0; i < row ; i++){
			
			int x = i;
			char ch =(char)( 'A' + x);

			for(int j = 0; j <= i ;j++){
				
				System.out.print(ch + " ");
				ch--;
			}
			System.out.println();
		}
	}

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter row :");
		int row = sc.nextInt();

		Program2 obj = new Program2();
		obj.printPattern(row);

	}
}



