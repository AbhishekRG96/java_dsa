/*
Que 5: WAP to toggle the String to uppercase or lowercase
Input: Java output: jAVA
Input: data output: DATA 
*/

import java.util.*;

class Program5{

	static String toUlterCase(char[] str){

		for(int i = 0; i < str.length ;i++){

			if((str[i] >= 'A' && str[i] <= 'Z')){

				str[i] = (char)(str[i] + 32);
			
			}else if((str[i] >= 'a' && str[i] <= 'z')){

                                str[i] = (char)(str[i] - 32);
                        }

		}
		String str1 = new String(str);
		return str1;
	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter string : ");
		String string = sc.next();

		char[] str = string.toCharArray();

		String str1 = toUlterCase(str);
		System.out.println(str1);


	}
}
