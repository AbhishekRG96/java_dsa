/*
 Que 3: WAP to find the factorial of a given number.
 */

import java.util.*;

class Program3{

	static int factorial(int num){
	
		int fact = 1;
		for(int i = num ; i > 0 ; i--){
		
			fact = fact * i;
		}
		return fact;
	} 

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number : ");
		int num = sc.nextInt();
	
		int fact = factorial(num);
		System.out.println(fact);

	}
}
