/*
 Que 2: WAP to print the following pattern
Take row input from the user
a
A B
a b c
A B C D
*/

import java.util.*;

class Program2{

	void printPattern(int row){

		for(int i = 0; i < row ; i++){

			char ch = 'A';
			char ch2 = 'a';
			for(int j = 0; j <= i ;j++){
	
				if(i % 2 == 1){
					System.out.print(ch + " ");
					ch++;
				}else{
					System.out.print(ch2 + " ");
                                        ch2++;
				}
			}
			System.out.println();
		}
	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter row :");
		int row = sc.nextInt();

		Program2 obj = new Program2();
		obj.printPattern(row);

	}
}

