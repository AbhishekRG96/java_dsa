/*
 Que 1: WAP to print the factorial of digits in a given range.
Input: 1-10
 */

import java.util.*;

class Program1{

	static void factorial(int start , int end){

		for(int j = start; j <= end ; j++){
			
			int fact = 1;
			for(int i = j ; i > 0 ; i--){

				fact = fact * i;
			}
			System.out.println(fact);
		}
	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter range : ");
		int start = sc.nextInt();
		int end = sc.nextInt();

		factorial(start,end);
		
	}
}

