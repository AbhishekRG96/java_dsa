/*
 Que 3: WAP to check whether the given number is a strong number or not.
 */

import java.util.*;

class Program3{

	static boolean isStrong(int num){

		int num2 = num;
		int sum = 0;

		while(num2 != 0){

			int rem = num2 % 10;
			num2 = num2 / 10;

			int fact = 1;
			for(int i = rem ; i > 0 ; i--){
	
				fact = fact * i;
			}
			sum = sum + fact;
		}
		if(sum == num){
			return true;
		}
		return false;
	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number : ");
		int num = sc.nextInt();

		boolean strong = isStrong(num);
		System.out.println(strong);

	}
}

