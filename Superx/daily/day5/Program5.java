/*
 Que 5: WAP to print the occurrence of a letter in given String.
Input String: “Know the code till the core”
Alphabet : o
Output: 3
 */

import java.util.*;

class Program5{

	static int occurance(char[] str1 , char ch){
	
		int count  = 0;
                for(int i = 0 ; i < str1.length ; i++){
                        
			if(str1[i] == ch){
                                count++;
			}
		}
                return count;
	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter string : ");
		String str = sc.next();

		System.out.println("Enter character : ");
                char ch = sc.next().charAt(0);

		char[] str1 = str.toCharArray();

		int count = occurance(str1,ch);
	   	System.out.println(count);	
	}

}
