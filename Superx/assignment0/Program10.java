/*
 Palindrome String 
 i/p -> malayalam
 */
import java.io.*;

class Program10 {

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the String");
              
	      	String str = br.readLine();

                char arr[] = str.toCharArray();

		int start = 0;
		int end = arr.length-1;
		int count = 0;
	
		while(start < end){
			
			if(arr[start] == arr[end]){
				start++;
				end--;
			}else{
				count++;
				break;
			}
		}

		if(count == 0){
			System.out.println("It is Palindrom String");
		}else{
			System.out.println("String is not a Palindrome String");
		}
	}
}
		



