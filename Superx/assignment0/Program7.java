/*
 Take the size of the array from the user crate two arrays of that size Initilize all second array elements as zero (0) for the first array take all element from the user check if the ele in the 1st array are even or not if its even then replace the value of the second array of that index with with 1 and print both the array
i/p = arr1: [4 2 3 6 8 7 1 0 9 5 ]
o/p -arr1:[4 2 3 6 8 7 1 0 9 5]
     arr2:[1 1 0 1 1 0 0 1 0 0]
*/ 

import java.io.*;

class Program7 {

	public static void main(String args[])throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the array Size");
		int size = Integer.parseInt(br.readLine());


		int arr1[] = new int[size];
		int arr2[] = new int[size];

		System.out.println("Enter the Array Elements");
		for(int i = 0;i<size;i++){
			arr1[i] = Integer.parseInt(br.readLine());
		}

		for(int i = 0 ; i <arr1.length ;i++){
		
			if(arr1[i] % 2 == 0){

				arr2[i] = 1;

			}
		}
		
		System.out.println("Array1");
		
		for(int x : arr1){
			System.out.print(x + ", ");
		}
		System.out.println();
		System.out.println("Array2");
		
		for(int y : arr2){
			System.out.print(y + ", ");
		}
	}
}

