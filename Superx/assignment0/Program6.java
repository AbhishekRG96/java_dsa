/*
 WAP to find no which has no its left is less than itself
i/p = 45675962
o/p = 9
*/ 

import java.util.*;

class Program6{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number : ");
		int num = sc.nextInt();

		int leftmax = Integer.MIN_VALUE;

		while(num != 0){
		
			int rem = num % 10;
			if(rem >= leftmax){
				leftmax = rem;
			}
			num = num / 10;
		}
		System.out.println("Left max number is : " + leftmax);
	}
}
