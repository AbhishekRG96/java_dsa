/*

   A  b  C  d  E
   e  D  c  B
   B  c  D
   d  C
   c

 */

import java.util.*;

class Program4 {

        public static void main(String args[]){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter the Row");
                int row = sc.nextInt();

                char ch = 'A';
                for(int i = 1;i<=row;i++){

                        for(int j = 1;j <=row-i+1;j++){

                                if(i % 2 == 1){

                                        if((i+j) % 2 == 0){
                                                System.out.print(ch + " ");
                                        }else{
                                                System.out.print((char)(ch+32)+" ");
                                        }
                                        ch++;
                                }else{
                                        ch--;
                                        if((i+j) % 2 == 0){
                                                System.out.print(ch + " ");
                                        }else{
                                                System.out.print((char)(ch+32)+ " ");
                                        }
                                }
                        }
                        System.out.println();
                }
        }
}

