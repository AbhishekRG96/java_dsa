/*
 Problem Description
Given an array A and an integer B, find the number of occurrences

of B in A.

Problem Constraints
1 <= B, Ai <= 109
1 <= length(A) <= 105

Example Input
Input 1:
A = [1, 2, 2], B = 2
Input 2:
A = [1, 2, 1], B = 3

Example Output
Output 1:
2
Output 2:
0

Example Explanation
Explanation 1:
Element at index 2, 3 is equal to 2 hence count is 2.
Explanation 2:
There is no element equal to 3 in the array.
 */

import java.io.*;

class Program2{

	static int countOf(int[] arr , int num){
	
		int count = 0;
		for(int i = 0 ;i < arr.length ;i++){
		
			if(arr[i] == num){
				count++;
			}
		}
		return count;
	}

	public static void main(String[] args)throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter the Array Size");
		int size = Integer.parseInt(obj.readLine());
		int arr[] = new int[size];

		System.out.println("Enter the Array Elements");
		for(int i = 0;i<arr.length;i++){
		
			arr[i]=Integer.parseInt(obj.readLine());
		
		}

		System.out.println("Enter the Element to count in array : ");
		int num = Integer.parseInt(obj.readLine());

		System.out.println("Count of " + num + " is " + countOf(arr,num));
	}
}
