/*
 Q3. Range Sum Query

Problem Description
- You are given an integer array A of length N.
- You are also given a 2D integer array B with dimensions M x 2, where

each row

denotes a [L, R] query.
- For each query, you have to find the sum of all elements from L to R

indices

in A (0 - indexed).
- More formally, find A[L] + A[L + 1] + A[L + 2] +... + A[R - 1] + A[R] for each
query.

Problem Constraints
1 <= N, M <= 103
1 <= A[i] <= 105
0 <= L <= R < N

Example Input
Input 1:
A = [1, 2, 3, 4, 5]
B = [[0, 3], [1, 2]]
Input 2:
A = [2, 2, 2]
B = [[0, 0], [1, 2]]

Example Output
Output 1:
[10, 5]

Output 2:
[2, 4]

Example Explanation
Explanation 1:
The sum of all elements of A[0 ... 3] = 1 + 2 + 3 + 4 = 10.
The sum of all elements of A[1 ... 2] = 2 + 3 = 5.
Explanation 2:
The sum of all elements of A[0 ... 0] = 2 = 2.
The sum of all elements of A[1 ... 2] = 2 + 2 = 4.
 */

import java.io.*;

class Program3{

	static int[] sumOfLR(int[] arr , int darr[][]){

		int[] prefixsumarr = new int[arr.length];

		prefixsumarr[0] = arr[0];
		for(int i = 1 ; i < arr.length ;i++){
		
			prefixsumarr[i] = prefixsumarr[i - 1] + arr[i];
		}

		int[] sumarr = new int[darr.length];

		for(int i = 0 ; i < darr.length ; i++){
		
			int start = darr[i][0];
			int end = darr[i][1];

			if(start > 0){
				sumarr[i] = prefixsumarr[end] - prefixsumarr[start - 1]; 
			}else{
				sumarr[i] = prefixsumarr[end];
			}
		}

		return sumarr;
	}

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter first Array Size");
		int size = Integer.parseInt(obj.readLine());

		System.out.println("Enter first Array Elements");
		int arr[] = new int[size];

		for(int i = 0;i<arr.length;i++){

			arr[i]=Integer.parseInt(obj.readLine());

		}

		System.out.println("Enter 2d Array size : ");
		int dsize = Integer.parseInt(obj.readLine());
                int darr[][] = new int[dsize][2];

                for(int i = 0;i<darr.length;i++){
			
			for(int j = 0 ; j < 2 ;j++){
                        	darr[i][j] = Integer.parseInt(obj.readLine());
			}
                }

		int[] sumarr = sumOfLR(arr , darr); 
		
		for(int x : sumarr){
			System.out.println(x +" ");
		}
	}
}
